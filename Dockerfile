FROM node:12.13-alpine as build
WORKDIR /usr/src/app
COPY . .
RUN npm install
RUN npm run build:prod

FROM php:7.2-apache
RUN a2enmod rewrite
COPY --from=build /usr/src/app/dist /var/www/html/admin
EXPOSE 80


