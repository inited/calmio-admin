import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Response } from '../../models/response.model';
import { StaticFilesService } from '../../services/static-files.service';
import { UtilsService } from '../../services/utils.service';


@Component({
    selector: 'app-static-files',
    templateUrl: './static-files.component.html',
    styleUrls: ['./static-files.component.css']
})
export class StaticFilesComponent implements OnInit {
    @ViewChild('templateErr') errorTemplate: any;
    public aboutContent: string;
    public termsContent: string;
    public loading: boolean;
    public languages: Array<object>;
    public selectedLanguage: string;
    public error: string;
    public modalRef: BsModalRef;

    constructor(private staticFilesService: StaticFilesService,
                private utilsService: UtilsService,
                private modalService: BsModalService) {
    }

    public ngOnInit(): void {
        this.languages = this.utilsService.getLanguages();
        this.selectedLanguage = 'cs';
        this.reload();
    }

    private async reload(): Promise<void> {
        await this.getAboutHtmlContent();
        await this.getTermsHtmlContent();
    }

    private async getAboutHtmlContent(): Promise<void> {
        this.loading = true;
        const response: any = await this.staticFilesService.getAboutStaticHtml(this.selectedLanguage);
        this.loading = false;
        if (response.status === 'ok') {
            this.aboutContent = response.data;
        } else {
            this.loading = false;
            this.error = response.data;
            this.modalRef = this.modalService.show(this.errorTemplate);
        }
    }

    private async getTermsHtmlContent(): Promise<void> {
        this.loading = true;
        const response: any = await this.staticFilesService.getTermsStaticHtml(this.selectedLanguage);
        this.loading = false;
        if (response.status === 'ok') {
            console.log(response);
            this.termsContent = response.data;
        } else {
            this.loading = false;
            this.error = response.data;
            this.modalRef = this.modalService.show(this.errorTemplate);
        }
    }

    public async saveTermsContent(): Promise<void> {
        this.loading = true;
        const response: Response = await this.staticFilesService.saveTermsStaticHtml(this.termsContent, this.selectedLanguage);
        this.loading = false;
        if (response.status === 'ok') {
            console.log(response);
        } else {
            this.error = response.data;
            this.modalRef = this.modalService.show(this.errorTemplate);
        }
    }

    public async saveAboutContent(): Promise<void> {
        this.loading = true;
        const response: Response = await this.staticFilesService.saveAboutStaticHtml(this.aboutContent, this.selectedLanguage);
        this.loading = false;
        if (response.status === 'ok') {
            console.log(response);
        } else {
            this.error = response.data;
            this.modalRef = this.modalService.show(this.errorTemplate);
        }
    }

    public async onChange(value: string): Promise<void> {
        this.selectedLanguage = value;
        await this.reload();
    }
}
