import { Component, ElementRef, EventEmitter, HostListener, Input, Output, ViewChild } from '@angular/core';

@Component({
    selector: 'app-drag-drop',
    templateUrl: './drag-drop.component.html',
    styleUrls: ['./drag-drop.component.css']
})
export class DragDropComponent {

    @Input() disabled: boolean = false;
    @Input() large: boolean = false;
    @Output() changed: EventEmitter<File[]> = new EventEmitter();
    @ViewChild('fileInput') fileInput: ElementRef<HTMLInputElement>;
    active: boolean = false;

    constructor() { }

    onChange(): void {
        this.changed.emit(Array.from(this.fileInput.nativeElement.files));
        this.fileInput.nativeElement.value = '';
    }

    @HostListener('dragover', ['$event'])
    onDragOver(event: DragEvent): void {
        event.stopPropagation();
        event.preventDefault();
        this.active = true;
    }

    @HostListener('dragleave', ['$event'])
    onDragLeave(): void {
        this.active = false;
    }

    @HostListener('drop', ['$event'])
    onDrop(event: DragEvent): void {
        event.preventDefault();
        this.active = false;
        if (this.disabled) {
            return;
        }
        this.changed.emit(Array.from(event.dataTransfer.files));
    }

    @HostListener('body:dragover', ['$event'])
    onBodyDragOver(event: DragEvent): void {
        event.preventDefault();
    }

    @HostListener('body:drop', ['$event'])
    onBodyDrop(event: DragEvent): void {
        event.preventDefault();
    }

    onBrowseClick(): void {
        if (this.disabled) {
            return;
        }
        this.fileInput.nativeElement.click();
    }
}
