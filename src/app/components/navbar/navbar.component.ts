import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../../services/users.service';


@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
    @Input() public login: boolean;

    constructor(private usersService: UsersService, private router: Router) {
    }

    public logout(): void {
        this.usersService.logout();
        this.router.navigate(['login']);
    }
}
