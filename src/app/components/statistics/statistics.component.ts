import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UserStatistics } from '../../models/statistic.model';
import { StatisticService } from '../../services/statistic.service';


@Component({
    selector: 'app-statistics',
    templateUrl: './statistics.component.html',
    styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
    @ViewChild('exportTable') public table: ElementRef;
    @ViewChildren('courseExportTables') public courseTables: QueryList<any>;
    @ViewChild('userExperienceExportTables') public userExperienceTable: any;
    @ViewChild('templateErr') public templateError: ElementRef;


    public storesData: Array<number> = [246, 222, 214, 208, 212, 235, 237, 216, 215, 215, 215, 200, 230, 213, 202, 222, 229,
        220, 206, 198, 205, 193, 192, 225, 188, 196, 212, 221, 216, 186, 201, 208, 184, 217, 205, 201, 193, 190,
        205, 197, 198, 192, 140, 200, 195, 170, 193, 206, 173, 191, 202, 175, 192, 98, 167, 191, 149, 129, 178,
        185, 177, 180, 180, 184, 92, 44, 117, 106, 83, 86, 116, 113, 100, 99, 107, 113, 97, 108, 100, 109, 104,
        84, 74, 82, 78, 79, 100, 68, 73, 105, 93, 41, 40, 37, 48, 48, 32, 31, 32, 44, 41, 31, 29, 35, 42, 27, 37,
        26, 28, 35, 26, 36];
    public startDate: moment.Moment = moment(new Date(2019, 6, 1));
    public endDate: moment.Moment = moment(new Date(2019, 9, 20));
    public activePage: number = 1;
    public itemsPerPage: number = 50;
    public page: number = 1;

    public dateLocale: any = {
        applyLabel: 'OK',
        format: 'D.M.YYYY',
        weekLabel: 'T',
        cancelLabel: 'Zrušit',
        clearLabel: 'Vyčistit',
        customRangeLabel: 'Vlastní',
        daysOfWeek: ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
        monthNames: ['Led', 'Úno', 'Bře', 'Dub', 'Kvě', 'Čer', 'Čvc', 'Srp', 'Zář', 'Říj', 'Lis', 'Pro'],
        firstDay: 1
    };
    public selectedDate: any = {
        startDate: undefined,
        endDate: undefined,
    };

    public loading: boolean;
    public errorMessage: string;
    public userStats: Array<UserStatistics>;
    public modalRef: BsModalRef;
    public total: number;
    public userPerPageCollapse: Array<boolean>;


    constructor(private statisticService: StatisticService,
                private modalService: BsModalService,
                private router: Router,
                private storage: Storage) {
        const today = new Date();
        this.selectedDate.endDate = moment(new Date(today.getFullYear(), today.getMonth(), today.getDate()));
        this.selectedDate.startDate = moment(this.startDate);
    }

    public async ngOnInit(): Promise<void> {
        this.getStatistics(this.getRequestParams());
    }

    public onChoosedDate(date: any): void {
        console.log(date, this.selectedDate);
    }


    public getStoreCount(platform: 'android' | 'ios', type: 'view' | 'download' | 'signup' | 'sub'): number {
        const DAY_MS = 24 * 60 * 60 * 1000;
        const ANDROID_VIEW = 0.4104897;
        const IOS_VIEW = 0.58951025;
        const IOS_DOWN = 0.09093460566938;
        const ANDROID_DOWN = 0.110036275695284;
        const ANDROID_SIGNUP = 0.104393389762193;
        const IOS_SIGNUP = 0.075498175694639;
        const ANDROID_SUB = 0.012494961708988;
        const IOS_SUB = 0.020488352511928;
        const startDate = this.selectedDate.startDate.valueOf() > this.startDate.valueOf()
            ? this.selectedDate.startDate : this.startDate;
        const endDate = this.selectedDate.endDate.valueOf() < this.endDate.valueOf()
            ? this.selectedDate.endDate : this.endDate;
        const startIndex = Math.floor(startDate.valueOf() - this.startDate.valueOf()) / DAY_MS;
        const endIndex = startIndex + Math.floor((endDate.valueOf() - startDate.valueOf()) / DAY_MS);
        let total = 0;
        for (let i = startIndex; i <= endIndex; i++) {
            if (this.storesData[i]) {
                total += this.storesData[i];
            }
        }
        if (platform === 'android') {
            total *= ANDROID_VIEW;
            switch (type) {
                case 'download':
                    total *= ANDROID_DOWN;
                    break;
                case 'signup':
                    total *= ANDROID_SIGNUP;
                    break;
                case 'sub':
                    total *= ANDROID_SUB;
                    break;
            }
        } else {
            total *= IOS_VIEW;
            switch (type) {
                case 'download':
                    total *= IOS_DOWN;
                    break;
                case 'signup':
                    total *= IOS_SIGNUP;
                    break;
                case 'sub':
                    total *= IOS_SUB;
                    break;
            }
        }
        return Math.floor(total);
    }

    public getLessonRowGroups(lessons: Array<any>): any[] {
        const chunk = 5;
        const batches = [];
        for (let i = 0, j = lessons.length; i < j; i += chunk) {
            batches.push(lessons.slice(i, i + chunk));
        }
        return batches;
    }

    private toggleCollapse(user: UserStatistics, position: number): void {
        if (user.courses.length > 0) {
            this.userPerPageCollapse[position] = !this.userPerPageCollapse[position];
        }
    }

    private async getStatistics(params: any): Promise<void> {
        this.loading = true;
        this.userPerPageCollapse = new Array(this.itemsPerPage).fill(false);

        const response: any = await this.statisticService.getUserStatistics(params);
        this.loading = false;
        if (response.status === 'ok') {
            this.userStats = response.data.data;
            if (!this.total) {
                this.total = response.data.totalCount;
            }
            console.log(this.userStats);
        } else {
            this.handleErrors(response);
        }
    }

    private getRequestParams(): any {
        const params = {};
        params[`offset`] = (this.page - 1) * this.itemsPerPage + 1;
        params[`limit`] = this.itemsPerPage;
        return params;
    }

    public onPageChange(p: number): void {
        this.page = p;
        const params = this.getRequestParams();
        this.getStatistics(params);
        window.scrollTo(0, 0);
    }

    public async getExport(): Promise<void> {
        this.loading = true;
        const response: any = await this.statisticService.getUserStatisticsExport();
        this.loading = false;
        if (response.status === 'ok') {
            const downloadURL = window.URL.createObjectURL(response.data);
            const link = document.createElement('a');
            link.href = downloadURL;
            link.download = 'export.xlsx';
            link.click();
        } else {
            await this.handleErrors(response);
        }
    }

    private async handleErrors(response: any): Promise<void> {
        if (response.status === 'unauthorized') {
            await this.storage.set('user', undefined);
            this.router.navigate(['login']);
        } else {
            this.errorMessage = 'Network error';
            this.modalRef = this.modalService.show(this.templateError);
        }
    }

}
