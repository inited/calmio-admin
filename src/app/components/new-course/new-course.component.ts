import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs/Subject';
import { Voice } from '../../models/voice.model';
import { VoicesService } from '../../services/voices.service';
import { Course, Intro } from '../../models/course.model';
import { Language } from '../../models/language.model';
import { Lesson } from '../../models/lesson.model';
import { Option } from '../../models/option.model';
import { ProgressSubject } from '../../models/progressSubject.model';
import { Response } from '../../models/response.model';
import { Source } from '../../models/source.model';
import { CoursesService } from '../../services/courses.service';
import { UtilsService } from '../../services/utils.service';


@Component({
    selector: 'app-new-course',
    templateUrl: './new-course.component.html',
    styleUrls: ['./new-course.component.css']
})
export class NewCourseComponent implements OnInit {
    @ViewChild('templateInvalidFormErr') templateInvalidFormErr: TemplateRef<any>;
    @ViewChild('templateConfirm') templateDeleteConfirm: TemplateRef<any>;
    public workingAdd: boolean = false;
    public subsjectAdd: Subject<number> = new Subject<number>();
    public activeUpload: {
        currentFileOrder: number;
        totalFiles: number;
    };
    public subjectLessonArr: Array<ProgressSubject> = [];
    public course: Course;
    public lesson: Lesson;
    public loading: boolean;
    public uploading: boolean;
    private params: Params;
    public errorMsg: string;
    public languages: Array<Language>;
    public languagesObject: any;
    public availableVoices: {
        all: Voice[],
        man: Voice[],
        woman: Voice[],
    };
    public voiceGenderSelectModel: {
        [key: string]: 'man' | 'woman',
    };
    public toRemove: string;
    public toRemoveText: string;
    public toRemovePosition: number;
    public toRemoveLessonPosition: number;
    public sourceLength: Array<string> = [];
    public modalRef: BsModalRef;
    public price: Array<Option> = [
        { value: true, text: 'Zdarma' },
        { value: false, text: 'Placený' }
    ];

    public requiredFields: any = {
        name: false,
        language: false,
        lessons: [],
        lesson: {
            order: false,
        }
    };
    public missingValues: Array<string> = [];


    constructor(
        private coursesService: CoursesService,
        private storage: Storage,
        private router: Router,
        private utilsService: UtilsService,
        private modalService: BsModalService,
        private route: ActivatedRoute,
        private voicesService: VoicesService,
    ) {
        this.languages = this.utilsService.getLanguages();
        this.generateLanguagesDict();
        this.course = new Course();
        // default value set to free
        this.course.free = true;
        this.course.lessons = [];
        this.lesson = new Lesson();
        this.lesson.intro = new Intro();
        this.lesson.type = 'audio';
    }


    private generateLanguagesDict(): void {
        this.languagesObject = {};
        this.languages.forEach(l => {
            this.languagesObject[l.value] = false;
        });
    }

    public async ngOnInit(): Promise<void> {
        await this.route.queryParams.subscribe((params: Params) => {
            this.params = params;
            console.log(this.params);
        });
        if (this.params.isNew === '0') {
            await this.getCourseDetail(this.params.id);
        }
        const voices = (await this.voicesService.getAll()).data;
        this.availableVoices = {
            all: voices,
            man: voices.filter(v => v.gender === 'man'),
            woman: voices.filter(v => v.gender === 'woman'),
        };
        this.prepVoiceGenderSelectModel();
    }

    private prepVoiceGenderSelectModel(): void {
        this.voiceGenderSelectModel = {};
        this.lesson.source.forEach((source, sIndex) => {
            this.voiceGenderSelectModel[`X-${sIndex}`] = source.voice as any || 'woman';
        });
        this.course.lessons.forEach((lesson, lIndex) => {
            lesson.source.forEach((source, sIndex) => {
                this.voiceGenderSelectModel[`${lIndex}-${sIndex}`] = source.voice as any || 'woman';
            });
        });
    }

    public onVoiceGenderChange(key: string, source: Source): void {
        const gender = this.voiceGenderSelectModel[key];
        source.voice = gender;
        source.lecturer = null;
    }

    public onSourceVoiceChange(source: Source): void {
        // tslint:disable-next-line triple-equals
        if (source.lecturer != undefined) {
            const voice = this.availableVoices.all.find(v => v.id === source.lecturer);
            source.voice = voice.gender;
        }
    }

    private resetLessonForm(): void {
        this.lesson = new Lesson();
    }

    private getLanguagesFromObject(): Array<string> {
        const keys = Object.keys(this.languagesObject);
        return keys.filter((l: string) => {
            return this.languagesObject[l];
        });
    }

    private resetRequiredFields(): void {
        this.requiredFields.name = false;
        this.requiredFields.language = false;
        this.requiredFields.lesson = {
            order: false
        };
    }

    private validateRequiredFields(): boolean {
        let valid = true;
        this.missingValues = [];
        this.resetRequiredFields();
        if (!this.course.name) {
            this.requiredFields.name = true;
            this.missingValues.push('Titulek');
            valid = false;
        }
        if (this.course.languages.length === 0) {
            this.requiredFields.language = true;
            this.missingValues.push('Jazyk');
            valid = false;
        }
        let i = 0;
        this.course.lessons.forEach(lesson => {
            const l = { idx: i, order: false };
            if (!lesson.order) {
                console.log('lesson:val:', l);
                l.order = true;
                this.missingValues.push(`Číslo lekce u lekce č. ${i + 1}`);
                valid = false;
            }
            lesson.source.forEach((src, srcIndex) => {
                if (!src.lecturer || !src.voice) {
                    this.missingValues.push(`Lektor u lekce č. ${i + 1}, varianta ${srcIndex + 1}`);
                    valid = false;
                }
                if (!src.title) {
                    this.missingValues.push(`Délka lekce u lekce č. ${i + 1}, varianta ${srcIndex + 1}`);
                    valid = false;
                }
            });
            this.requiredFields.lessons.push(l);
            i++;
        });
        console.log(this.requiredFields);
        return valid;
    }

    public async saveCourse(templateOk: any): Promise<any> {
        if (this.lesson.order) {
            const success: boolean = this.addLessonToCourse();
            if (!success) {
                return;
            }
        }
        this.loading = true;
        let response: any;
        this.course.languages = this.getLanguagesFromObject();
        if (!this.validateRequiredFields()) {
            this.loading = false;
            this.modalRef = this.modalService.show(this.templateInvalidFormErr);
            return;
        }
        if (this.params.isNew === '0') {
            response = await this.coursesService.updateCourse(this.course);
        } else {
            response = await this.coursesService.newCourse(this.course);
        }
        this.loading = false;

        if (response.status === 'ok') {
            this.modalRef = this.modalService.show(templateOk);
        }
    }

    public async getCourseDetail(id: any): Promise<void> {
        this.loading = true;
        const response: any = await this.coursesService.getCourseById(id);
        this.loading = false;
        if (response.status === 'ok') {
            this.course = response.data;
            if (!this.validateRequiredFields()) {
                console.log('error load');
            }
            this.course.lessons.sort((a: Lesson, b: Lesson) => (a.order > b.order) ? 1 : -1);
            this.course.lessons.forEach(
                () => {
                    const item: ProgressSubject = {
                        subject: new Subject<number>(),
                        working: false
                    };
                    this.subjectLessonArr.push(item);
                }
            );
            this.course.languages.forEach(l => {
                this.languagesObject[l] = true;
            });

            console.log(this.course);
        } else if (response.status === 'unauthorized') {
            await this.storage.set('user', undefined);
            this.router.navigate(['login']);
        } else {
            console.log(response.status);
        }
    }

    public close(): void {
        this.modalRef.hide();
        this.router.navigate(['/courses']);
    }

    public photoChange(files: File[], tmp: any, type: string, position?: number): void {
        this.uploadFileImg(tmp, files[0], type, position);
    }

    public async uploadFileImg(template: TemplateRef<any>, imgFile: File, type: string, position?: number): Promise<void> {
        const response: Response = await this.utilsService.uploadFile(imgFile, new Subject<number>());
        if (response.status === 'ok') {
            if (type === 'intro') {
                if (position < 0) {
                    this.lesson.intro.video = response.data.url;
                } else {
                    this.course.lessons[position].intro.video = response.data.url;
                }
            } else {
                this.course.image = response.data.url;
                this.course.thumbnail = response.data.thumbnailUrl;
            }
            console.log('ok');
        } else {
            this.errorMsg = 'Network error';
            this.modalRef = this.modalService.show(template);
        }
    }

    public addLessonToCourse(): boolean {
        this.missingValues = [];
        let valid = true;
        if (!this.lesson.order) {
            this.requiredFields.lesson.order = true;
            valid = false;
            this.missingValues.push('Číslo lekce.');
        }
        this.lesson.source.forEach((src, srcIndex) => {
            if (!src.lecturer || !src.voice) {
                this.missingValues.push(`Lektor u varianty ${srcIndex + 1}.`);
                valid = false;
            }
            if (!src.title) {
                this.missingValues.push(`Délka lekce u varianty ${srcIndex + 1}.`);
                valid = false;
            }
        });
        if (!valid) {
            this.modalRef = this.modalService.show(this.templateInvalidFormErr);
            return valid;
        }
        this.lesson.source.forEach(s => {
            s.title = s.title.toString();
        });
        this.course.lessons.push(this.lesson);
        console.log(this.course);
        this.requiredFields.lessons.push({ order: false });
        this.subjectLessonArr.push({ subject: new Subject<number>(), working: false });
        this.resetLessonForm();
        this.prepVoiceGenderSelectModel();
        return valid;
    }

    public removeLesson(i: number): void {
        this.requiredFields.lessons.slice(i, 1);
        this.course.lessons.splice(i, 1);
        this.prepVoiceGenderSelectModel();
    }

    public removePhoto(type: string, position?: number): void {
        if (type === 'intro') {
            if (position < 0) {
                this.lesson.intro.video = '';
                this.lesson.intro.text = '';
            } else {
                this.course.lessons[position].intro.text = '';
                this.course.lessons[position].intro.video = '';
            }
        } else {
            this.course.thumbnail = '';
            this.course.image = '';
        }
    }

    public async addLessonFiles(files: File[], template: TemplateRef<any>, indexLesson: number = -1): Promise<void> {
        this.activeUpload = {
            currentFileOrder: 1,
            totalFiles: files.length,
        };
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < files.length; i++) {
            const file = files[i];
            const success = await this.addLessonFile(file, template, indexLesson);
            if (!success) {
                break;
            }
            this.activeUpload.currentFileOrder += 1;
        }
    }

    public async addLessonFile(lessonFile: File, template: TemplateRef<any>, indexLesson: number = -1): Promise<boolean> {
        this.uploading = true;
        let subject: Subject<number>;
        if (indexLesson > -1) {
            this.subjectLessonArr[indexLesson].working = true;
            subject = this.subjectLessonArr[indexLesson].subject;
        } else {
            this.workingAdd = true;
            subject = this.subsjectAdd;
        }
        const response: Response = await this.utilsService.uploadFile(lessonFile, subject);
        console.log(response);
        console.log('3', this.lesson.source);
        if (indexLesson > -1) {
            this.subjectLessonArr[indexLesson].working = false;
        } else {
            this.workingAdd = false;
        }
        this.uploading = false;

        if (response.hasOwnProperty('status') && response.status === 'ok') {
            if (indexLesson === -1) {
                this.lesson.source.push(new Source('', response.data.url));
            } else {
                this.course.lessons[indexLesson].source.push(new Source('', response.data.url));
            }
            console.log('ok');
            this.prepVoiceGenderSelectModel();
            return true;
        } else {
            this.errorMsg = 'Network error';
            this.modalRef = this.modalService.show(template);
            return false;
        }
    }

    public removeSourceFromLesson(lessonIndex: number, sourceIndex: number): void {
        if (lessonIndex !== null && lessonIndex !== undefined) {
            this.course.lessons[lessonIndex].source.splice(sourceIndex, 1);
        } else {
            this.lesson.source.splice(sourceIndex, 1);
        }
        this.prepVoiceGenderSelectModel();
    }

    public getSubject(idx: number): Subject<number> {
        return this.subjectLessonArr[idx].subject;
    }

    public openRemoveModal(type: string, text: string, removePosition: number = -1, removeLessonPosition: number = null): void {
        this.toRemoveText = text;
        this.toRemovePosition = removePosition;
        this.toRemoveLessonPosition = removeLessonPosition;
        this.toRemove = type;
        this.modalRef = this.modalService.show(this.templateDeleteConfirm);
    }

    public removeHandler(): void {
        switch (this.toRemove) {
            case 'photo':
                this.removePhoto('photo');
                break;
            case 'intro':
                this.removePhoto('intro', this.toRemovePosition);
                break;
            case 'removeSource':
                this.removeSourceFromLesson(this.toRemoveLessonPosition, this.toRemovePosition);
                break;
            case 'removeLesson':
                this.removeLesson(this.toRemovePosition);
                break;
        }
        this.modalRef.hide();
    }
}
