import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Meditation } from '../../models/meditation.model';
import { Response } from '../../models/response.model';
import { MeditationService } from '../../services/meditation.service';
import { UsersService } from '../../services/users.service';


@Component({
    selector: 'app-courses',
    templateUrl: './meditations.component.html',
    styleUrls: ['./meditations.component.css']
})
export class MeditationsComponent implements OnInit {
    @ViewChild('templateErr') public errorTemplate: any;
    public modalRef: BsModalRef;
    public errorMessage: string;
    public meditationToDelete: Meditation;
    public meditations: Array<Meditation>;
    public loading: boolean;

    constructor(private meditationService: MeditationService,
                private router: Router,
                private storage: Storage,
                private usersService: UsersService,
                private modalService: BsModalService) {
        if (!this.usersService.getToken()) {
            router.navigate(['login']);
        }
    }

    public ngOnInit(): void {
        this.getMeditations();
    }

    public async getMeditations(): Promise<void> {
        this.loading = true;
        const response: Response = await this.meditationService.getCourses();
        this.loading = false;
        if (response.status === 'ok') {
            this.meditations = response.data;
        } else {
            this.errorMessage = response.status;
            this.modalRef = this.modalService.show(this.errorTemplate);
        }
    }

    public async newMeditation(): Promise<void> {
        await this.router.navigate(['/new-meditation']);
    }

    public async modify(course: Meditation): Promise<void> {
        await this.router.navigate(['/new-meditation'], { queryParams: { id: course.id, isNew: 0 } });
    }

    public delete(course: Meditation, template: TemplateRef<any>): void {
        this.meditationToDelete = course;
        this.modalRef = this.modalService.show(template);
    }

    public async scheduleTomorrow(course: Meditation): Promise<void> {
        this.loading = true;
        const response: Response = await this.meditationService.scheduleTomorrow(course.id);
        this.loading = false;
        if (response.status === 'ok') {
            this.meditations = this.meditations.map(med => ({
                ...med,
                tomorrowPreferredDailyMeditation: med.id === course.id,
            }));
        } else {
            this.errorMessage = response.status;
            this.modalRef = this.modalService.show(this.errorTemplate);
        }
    }

    public async deleteConfirmed(template: TemplateRef<any>): Promise<void> {
        this.modalRef.hide();
        this.loading = true;
        const response: Response = await this.meditationService.deleteCourse(this.meditationToDelete.id);
        this.loading = false;
        console.log(response);
        if (response.status === 'ok') {
            const index: number = this.meditations.indexOf(this.meditationToDelete);
            this.meditations.splice(index, 1);
        } else if (response.status === 'unauthorized') {
            await this.storage.set('user', undefined);
            await this.router.navigate(['login']);
        } else {
            this.errorMessage = 'Network error';
            this.modalRef = this.modalService.show(template);
        }
    }

}
