import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UsersService } from '../../services/users.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    public userName: string = '';
    public password: string = '';
    public errorMessage: string = '';
    public modalRef: BsModalRef;
    public loading: boolean = false;

    constructor(private usersService: UsersService,
                private storage: Storage,
                private router: Router,
                public modalService: BsModalService) {
    }

    public ngOnInit(): void {
    }

    public async login(template: TemplateRef<any>): Promise<void> {
        this.loading = true;
        const response: any = await this.usersService.login(this.userName, this.password);
        this.loading = false;
        console.log(response);
        if (response.status === 'ok') {
            await this.storage.set('user', response.user);
            await this.router.navigate(['courses']);
        } else if (response.status === 'notActivated') {
            this.errorMessage = 'Your account was not activated yet.';
            this.modalRef = this.modalService.show(template);
        } else if (response.status === 'wrongCredentials') {
            this.errorMessage = 'Wrong login credentials';
            this.modalRef = this.modalService.show(template);
        } else {
            this.errorMessage = 'Network error';
            this.modalRef = this.modalService.show(template);
        }
    }
}
