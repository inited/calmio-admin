import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { mod } from 'ngx-bootstrap/chronos/utils';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Code } from '../../models/code.model';
import { Partner } from '../../models/partner.model';
import { Response } from '../../models/response.model';
import { PartnerService } from '../../services/partner.service';
import { UsersService } from '../../services/users.service';

@Component({
    selector: 'app-partners',
    templateUrl: './partners.component.html',
    styleUrls: ['./partners.component.css']
})
export class PartnersComponent implements OnInit {
    @ViewChild('templateErr') public errorTemplate: any;
    public modalRef: BsModalRef;
    public errorModalRef: BsModalRef;
    public errorMessage: string;
    public partnerToDelete: Partner;
    public partners: Array<Partner>;
    public loading: boolean;
    public addEditTitle: string;
    public partner: Partner;
    public addEditCodeTitle: string;
    public code: Code;
    public partnerCodeEdit: Partner;


    constructor(private partnerService: PartnerService,
                private router: Router,
                private storage: Storage,
                private usersService: UsersService,
                private modalService: BsModalService) {
        if (!this.usersService.getToken()) {
            router.navigate(['login']);
        }
    }

    public async ngOnInit(): Promise<void> {
        await this.getPartners();
    }

    public async getPartners(): Promise<void> {
        this.loading = true;
        const response: Response = await this.partnerService.getPartners();
        this.loading = false;
        if (response.status === 'ok') {
            this.partners = response.data;
        } else {
            this.handleError(response);
        }
    }

    public deletePartner(partner: Partner, template: TemplateRef<any>): void {
        this.partnerToDelete = partner;
        this.modalRef = this.modalService.show(template);
    }

    public deleteCode(partner: Partner, template: TemplateRef<any>): void {
        this.partnerCodeEdit = partner;
        this.modalRef = this.modalService.show(template);
    }

    public async deletePartnerConfirmed(): Promise<void> {
        this.modalRef.hide();
        this.loading = true;
        const response: Response = await this.partnerService.deletePartner(this.partnerToDelete.id);
        if (response.status === 'ok') {
            this.partners = response.data;
            await this.getPartners();
        } else {
            this.handleError(response);
        }
        this.loading = false;
    }

    public async deleteCodeConfirmed(): Promise<void> {
        this.modalRef.hide();
        this.loading = true;
        const response = await this.partnerService.deleteCode(this.partnerCodeEdit.code, this.partnerCodeEdit.id);
        if (response.status === 'ok') {
            await this.getPartners();
        } else {
            this.handleError(response);
        }
        this.loading = false;
    }

    public async saveCode(modify: boolean): Promise<void> {
        this.loading = true;
        let resultPromo;
        if (modify) {
            resultPromo = await this.partnerService.saveUpdatedCode(this.code, this.partnerCodeEdit.id);
        } else {
            resultPromo = await this.partnerService.saveNewCode(this.code, this.partnerCodeEdit.id);
        }
        this.loading = false;
        if (!resultPromo || resultPromo.status !== 'ok') {
            this.handleError(resultPromo);
            return;
        }
        this.modalRef.hide();
        await this.getPartners();
    }

    public async savePartner(modify: boolean = false): Promise<void> {
        this.loading = true;
        const result = await this.partnerService.savePartner(this.partner, modify);
        if (result.status === 'ok') {
            if (this.modalRef) {
                this.modalRef.hide();
            }
            console.log('Saved successfully');
            await this.getPartners();
        } else {
            this.handleError(result);
        }
        this.loading = false;
    }

    public openModal(newItem: boolean, template: TemplateRef<any>): void {
        if (newItem) {
            this.partner = new Partner();
            this.addEditTitle = 'Přidání partnera';
        }
        this.modalRef = this.modalService.show(template);
    }

    public openCodeModal(newItem: boolean, partner: Partner, template: TemplateRef<any>): void {
        if (newItem) {
            this.code = new Code();
            this.addEditCodeTitle = `Přidání promo kódu - ${partner.name}`;
        } else {
            this.addEditCodeTitle = `Editace promo kódu - ${partner.name}`;
            this.code = JSON.parse(JSON.stringify(partner.code));
        }
        this.partnerCodeEdit = partner;
        this.modalRef = this.modalService.show(template);
    }

    public modify(partner: Partner, template: TemplateRef<any>): void {
        this.partner = JSON.parse(JSON.stringify(partner));
        this.addEditTitle = `Partner - ${partner.name} - editovat`;
        this.openModal(false, template);
    }

    public generateLink(partner: Partner): string {
        if (partner.code) {
            return `https://partner.calmio.cz/partner/${partner.code.name}`;
        }
    }

    private handleError(result: Response): void {
        console.log(result);
        this.errorMessage = result.data;
        this.errorModalRef = this.modalService.show(this.errorTemplate);
    }
}
