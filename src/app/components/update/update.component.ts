import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Response } from '../../models/response.model';
import { Version } from '../../models/version.model';
import { UpdateService } from '../../services/update.service';

@Component({
    selector: 'app-update',
    templateUrl: './update.component.html',
    styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
    @ViewChild('templateErr') errorTemplate: TemplateRef<any>;
    public loading: boolean;
    public updatedVersions: Version;
    public versions: Version;
    public newVersion: string;
    public modalRef: BsModalRef;
    public errorMessage: string;


    constructor(private updateService: UpdateService,
                private modalService: BsModalService) {
    }

    public async ngOnInit(): Promise<void> {
        await this.getCurrentVersion();
        this.resetUpdatedVersions();
    }

    private resetUpdatedVersions(): void {
        this.updatedVersions = {
            latestVersion: '',
            forceUpdateVersion: ''
        };
    }

    private async getCurrentVersion(): Promise<void> {
        this.loading = true;
        const response: Response = await this.updateService.getCurrentVersion();
        this.loading = false;
        console.log(response);
        if (response.status === 'ok') {
            this.versions = response.data;
        } else {
            this.errorMessage = response.data.message;
            this.modalRef = this.modalService.show(this.errorTemplate);
        }
    }

    public async sendUpdateNotification(template: TemplateRef<any>): Promise<void> {
        this.loading = true;
        const response: Response = await this.updateService.sendUpdate(this.versions, this.updatedVersions);
        this.loading = false;
        console.log(response);
        if (response.status === 'ok') {
            console.log(response.data);
            await this.getCurrentVersion();
            this.resetUpdatedVersions();
            this.modalRef = this.modalService.show(template);
        } else {
            this.errorMessage = response.data.message;
            this.modalRef = this.modalService.show(this.errorTemplate);
        }
    }

    public closeModal(): void {
        this.newVersion = '';
        this.modalRef.hide();
    }
}
