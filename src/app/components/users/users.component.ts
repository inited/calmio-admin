import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UserModel } from '../../models/user.model';
import { UsersService } from '../../services/users.service';


@Component({
    selector: 'app-courses',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

    public modalRef: BsModalRef;
    public errorMessage: string;
    public userToDelete: UserModel;
    public users: Array<UserModel>;
    public testUser: boolean;
    public modalTitle: string;
    public selectedUser: UserModel;
    public loading: boolean;
    public page: number = 1;
    public itemsPerPage: number = 50;
    public adminUser: boolean;
    public total: number;

    @ViewChild('templateErr') public templateError: TemplateRef<any>;

    constructor(private usersService: UsersService,
                private router: Router,
                private storage: Storage,
                private modalService: BsModalService) {
        if (!this.usersService.getToken()) {
            router.navigate(['login']);
        }
    }

    public ngOnInit(): void {
        this.getUsers(this.getRequestParams());
    }


    public async getUsers(params: any): Promise<any> {
        this.loading = true;
        const response: any = await this.usersService.getUsers(params);
        this.loading = false;
        if (response.status === 'ok') {
            this.users = response.users.data;
            if (!this.total) {
                this.total = response.users.totalCount;
            }
        } else if (response.status === 'unauthorized') {
            await this.storage.set('user', undefined);
            this.router.navigate(['login']);
        } else {
            this.errorMessage = 'Network error';
            this.modalRef = this.modalService.show(this.templateError);
        }
    }

    public modify(user: UserModel, template: TemplateRef<any>): void {
        this.modalTitle = 'Modify UserModel';
        this.selectedUser = new UserModel();
        this.selectedUser.name = user.name;
        this.selectedUser.surname = user.surname;
        this.selectedUser.email = user.email;
        this.selectedUser.roles = user.roles;
        this.adminUser = user.roles.indexOf('ADMIN') > -1;
        this.testUser = user.roles.indexOf('TEST') > -1;
        this.modalRef = this.modalService.show(template);
        console.log(this.adminUser);
    }

    public delete(user: UserModel, template: any): void {
        this.userToDelete = user;
        this.modalRef = this.modalService.show(template);
    }

    public async deleteConfirmed(template: any): Promise<void> {
        this.modalRef.hide();
        this.loading = true;
        const response: any = await this.usersService.deleteUser(this.userToDelete);
        this.loading = false;
        console.log(response);
        if (response.status === 'ok') {
            const index: number = this.users.indexOf(this.userToDelete);
            this.users.splice(index, 1);
        } else if (response.status === 'unauthorized') {
            await this.storage.set('user', undefined);
            this.router.navigate(['login']);
        } else {
            this.errorMessage = 'Network error';
            this.modalRef = this.modalService.show(template);
        }
    }

    public async saveUserModification(errorTemplate: TemplateRef<any>): Promise<any> {
        this.loading = true;
        const response: any = await this.usersService.updateUser(this.selectedUser);
        this.loading = false;
        if (response.status === 'ok') {
            this.modalRef.hide();
            this.getUsers(this.getRequestParams());

        } else if (response.status === 'unauthorized') {
            await this.storage.set('user', undefined);
            this.router.navigate(['login']);
        } else {
            this.errorMessage = 'Network error';
            this.modalRef = this.modalService.show(errorTemplate);
        }
    }

    public toggleUserRole(): void {
        console.log(this.adminUser);
        const role: string = this.adminUser ? 'ADMIN' : 'USER';
        this.selectedUser.roles = this.testUser ? ['TEST', role] : [role];
        console.log(this.selectedUser.roles);

    }

    private getRequestParams(): any {
        const params = {};
        params[`offset`] = (this.page - 1) * this.itemsPerPage + 1;
        params[`limit`] = this.itemsPerPage;
        console.log(params);
        console.log(this.total);
        return params;
    }

    public onPageChange(p: number): void {
        this.page = p;
        const params = this.getRequestParams();
        this.getUsers(params);
        window.scrollTo(0, 0);
    }

}
