import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Course } from '../../models/course.model';
import { Response } from '../../models/response.model';
import { CoursesService } from '../../services/courses.service';
import { UsersService } from '../../services/users.service';


@Component({
    selector: 'app-courses',
    templateUrl: './courses.component.html',
    styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
    @ViewChild('templateErr') public errorTemplate: any;
    public modalRef: BsModalRef;
    public errorMessage: string;
    public courseToDelete: Course;
    public courses: Array<Course>;
    public loading: boolean;

    constructor(private coursesService: CoursesService,
                private router: Router,
                private storage: Storage,
                private usersService: UsersService,
                private modalService: BsModalService) {
        if (!this.usersService.getToken()) {
            router.navigate(['login']);
        }
    }

    public async ngOnInit(): Promise<void> {
        await this.getCourses();
    }

    public async getCourses(): Promise<void> {
        this.loading = true;
        const response: Response = await this.coursesService.getCourses();
        this.loading = false;
        if (response.status === 'ok') {
            this.courses = response.data;
        } else {
            this.errorMessage = response.data.message;
            this.modalRef = this.modalService.show(this.errorTemplate);
        }
    }

    public async newCourse(): Promise<void> {
        await this.router.navigate(['/new-course']);
    }

    public async modify(course: Course): Promise<void> {
        await this.router.navigate(['/new-course'], {queryParams: {id: course.id, isNew: 0}});
    }

    public delete(course: Course, template: TemplateRef<any>): void {
        this.courseToDelete = course;
        this.modalRef = this.modalService.show(template);
    }

    public async deleteConfirmed(): Promise<void> {
        this.modalRef.hide();
        this.loading = true;
        const response: Response = await this.coursesService.deleteCourse(this.courseToDelete.id);
        this.loading = false;
        console.log(response);
        if (response.status === 'ok') {
            const index: number = this.courses.indexOf(this.courseToDelete);
            this.courses.splice(index, 1);
        } else {
            console.log(response);
            this.errorMessage = response.data.message;
            this.modalRef = this.modalService.show(this.errorTemplate);
        }
    }
}
