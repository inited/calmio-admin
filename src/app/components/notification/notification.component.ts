import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { Notification } from '../../models/notification.model';
import { NotificationService } from '../../services/notification.service';

@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

    @ViewChild('templateErr') public errorTemplate: TemplateRef<any>;
    public notification: Notification;
    public notificationDuplicate: Notification;
    public notifications: Array<Notification> = [];
    public modalRef: BsModalRef;
    public titleToRepeat: string;
    public loading: boolean;
    public error: any;

    static getEmpty(): any {
        return {
            title: '',
            message: '',
            test: true
        };
    }

    constructor(protected modalService: BsModalService, protected notificationService: NotificationService) {
        this.notification = NotificationComponent.getEmpty();
    }

    public async ngOnInit(): Promise<any> {
        await this.getNotificationsHistory();
    }

    private async getNotificationsHistory(): Promise<any> {
        this.loading = true;
        const result: any = await this.notificationService.getNotifications();
        console.log(result);
        if (result.status === 'ok') {
            this.notifications = result.data;
        } else {
            console.log(result);
            this.error = result.data;
            this.modalRef = this.modalService.show(this.errorTemplate);
        }
        this.loading = false;
    }

    public async sendNotification(duplicate: boolean = false): Promise<any> {
        this.loading = true;
        let toSave: any;
        if (duplicate) {
            toSave = Object.assign({}, this.notificationDuplicate);
            toSave.test = true;
        } else {
            toSave = Object.assign({}, this.notification);
        }
        console.log(toSave);
        const result: any = await this.notificationService.sendNotification(toSave);
        if (result.status === 'ok') {
            console.log('Saved successfully');
            await this.getNotificationsHistory();
            if (!duplicate) {
                this.notification = NotificationComponent.getEmpty();
            }
        } else {
            console.log(result);
            this.error = result.data;
            this.modalRef = this.modalService.show(this.errorTemplate);
        }
        this.loading = false;
    }

    public async sendRepeatNotification(): Promise<any> {
        await this.sendNotification(true);
        this.notificationDuplicate = NotificationComponent.getEmpty();
        this.modalRef.hide();
    }

    public repeatNotification(notification: Notification, template: TemplateRef<any>): void {
        const ngModalOptions: ModalOptions = {
            backdrop: 'static',
            class: 'modal-dialog-centered modal-lg',
            keyboard: false,
        };
        this.titleToRepeat = notification.title;
        this.notificationDuplicate = Object.assign({}, notification);
        this.notificationDuplicate.test = true;
        this.modalRef = this.modalService.show(template, ngModalOptions);
    }
}
