import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { UtilsService } from '../../services/utils.service';
import { Response } from '../../models/response.model';
import { Voice } from '../../models/voice.model';
import { VoicesService } from '../../services/voices.service';

@Component({
    selector: 'app-voices',
    templateUrl: './voices.component.html',
    styleUrls: ['./voices.component.css']
})
export class VoicesComponent implements OnInit {

    @ViewChild('templateErr') public errorTemplate: TemplateRef<any>;
    @ViewChild('templateEdit') public editTemplate: TemplateRef<any>;
    @ViewChild('templateDelete') public deleteTemplate: TemplateRef<any>;
    @ViewChild('templateConfirmDeletePreview') public templateConfirmDeletePreview: TemplateRef<any>;
    @ViewChild('templateConfirmSave') public templateConfirmSave: TemplateRef<any>;

    public modalRef: BsModalRef;
    public modalRefSecondary: BsModalRef;
    public errorModalRef: BsModalRef;
    public errorMessage: string;
    public items: Voice[];
    public loading: boolean;
    public selected: Voice;
    public uploadSubject: Subject<number>;

    constructor(
        private voicesService: VoicesService,
        public modalService: BsModalService,
        private utilsService: UtilsService,
    ) {
    }

    public ngOnInit(): void {
        this.getAll();
    }

    public async getAll(): Promise<void> {
        this.loading = true;
        const response: Response = await this.voicesService.getAll();
        this.loading = false;
        if (response.status === 'ok') {
            this.items = response.data;
        } else {
            this.handleError(response);
        }
    }

    public onCreateClick(): void {
        this.selected = {
            id: undefined,
            name: '',
            gender: 'woman',
            previewUrl: undefined,
        };
        this.modalRef = this.modalService.show(this.editTemplate);
    }

    public onEditClick(item: Voice): void {
        this.selected = Object.assign({}, item);
        this.modalRef = this.modalService.show(this.editTemplate);
    }

    public onDeleteClick(item: Voice): void {
        this.selected = Object.assign({}, item);
        this.modalRef = this.modalService.show(this.deleteTemplate);
    }

    public onConfirmDelete(): void {
        this.delete(this.selected.id);
    }

    public onSaveClick(): void {
        this.modalRefSecondary = this.modalService.show(this.templateConfirmSave);
    }

    public onConfirmSaveClick(): void {
        this.save(this.selected, true);
        this.modalRefSecondary.hide();
    }

    public onRemovePreviewClick(): void {
        this.modalRefSecondary = this.modalService.show(this.templateConfirmDeletePreview);
    }

    public onConfirmDeletePreviewClick(): void {
        this.selected.previewUrl = null;
        if (this.selected.id) {
            this.save(this.selected, false);
        }
        this.modalRefSecondary.hide();
    }

    public async onAddVoicePreviewFile(files: File[], template: any): Promise<void> {
        const file = files[0];
        this.uploadSubject = new Subject<number>();
        const response: Response = await this.utilsService.uploadFile(file, this.uploadSubject);
        if (response.status === 'ok') {
            this.selected.previewUrl = response.data.url;
            if (this.selected.id) {
                this.save(this.selected, false);
            }
        } else {
           this.handleError(response);
        }
        this.uploadSubject = undefined;
    }

    private async delete(id: number): Promise<void> {
        this.modalRef.hide();
        this.loading = true;
        const response: Response = await this.voicesService.delete(id);
        this.loading = false;
        if (response.status === 'ok') {
            this.items = response.data;
            this.getAll();
        } else {
            this.handleError(response);
        }
    }

    private async save(item: Voice, hideModal: boolean): Promise<void> {
        this.loading = true;
        const result = item.id !== undefined
            ? await this.voicesService.save(item)
            : await this.voicesService.create(item);
        this.loading = false;
        if (result.status === 'ok') {
            if (hideModal) {
                this.modalRef.hide();
            }
            this.getAll();
        } else {
            this.handleError(result);
        }
    }

    private handleError(result: Response): void {
        this.errorMessage = result.data;
        this.errorModalRef = this.modalService.show(this.errorTemplate);
    }
}
