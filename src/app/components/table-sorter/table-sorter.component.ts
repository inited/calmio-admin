import { Component, Input, OnInit } from '@angular/core';
import { DataTable, SortEvent } from 'angular-9-datatable';

@Component({
    selector: 'app-table-sorter',
    templateUrl: './table-sorter.component.html',
    styleUrls: ['./table-sorter.component.css']
})
export class TableSorterComponent implements OnInit {

    @Input() public by: string;
    public isSortedByMeAsc: boolean = false;
    public isSortedByMeDesc: boolean = false;

    public constructor(private mfTable: DataTable) {
    }

    public ngOnInit(): void {
        this.mfTable.onSortChange.subscribe((event: SortEvent) => {
            this.isSortedByMeAsc = (event.sortBy === this.by && event.sortOrder === 'asc');
            this.isSortedByMeDesc = (event.sortBy === this.by && event.sortOrder === 'desc');
        });
    }

    public sort(): void {
        if (this.isSortedByMeAsc) {
            this.mfTable.setSort(this.by, 'desc');
        } else {
            this.mfTable.setSort(this.by, 'asc');
        }
    }

}
