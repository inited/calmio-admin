import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { UsersService } from './services/users.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    public title: string = 'app';

    constructor(
        private storage: Storage,
        private router: Router,
        private user: UsersService,
    ) {
        this.getUserInfo();
    }

    private async getUserInfo(): Promise<void> {
        const token: string = localStorage.getItem('token');
        if (token) {
            this.user.setToken(token);
        }
        if (window.location.pathname.replace('admin/', '').length > 1) {
            return;
        } else if (token) {
            this.router.navigate(['courses']);
        } else {
            this.router.navigate(['login']);
        }
    }
}
