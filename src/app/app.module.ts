import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Injectable, LOCALE_ID, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { IonicStorageModule } from '@ionic/storage';
import { NgxDataTableModule } from 'angular-9-datatable';
import { ProgressHttpModule } from 'angular-progress-http';
import { OwlDateTimeIntl, OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { LoadingModule } from 'ngx-loading';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { MeditationsComponent } from './components/meditations/meditations.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NewCourseComponent } from './components/new-course/new-course.component';
import { NewMeditationComponent } from './components/new-meditation/new-meditation.component';
import { NotificationComponent } from './components/notification/notification.component';
import { TableSorterComponent } from './components/table-sorter/table-sorter.component';
import { UsersComponent } from './components/users/users.component';
import localeCs from '@angular/common/locales/cs';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { CoursesComponent } from './components/courses/courses.component';
import { StaticFilesComponent } from './components/static-files/static-files.component';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { UpdateComponent } from './components/update/update.component';
import { ExperiencePipe } from './pipes/experience.pipe';
import { NoTestUserPipe } from './pipes/no-test-user.pipe';
import { TruncatePipe } from './pipes/truncate.pipe';
import { UsersWithCoursePipe } from './pipes/users-with-course.pipe';
import { CoursesService } from './services/courses.service';
import { MeditationService } from './services/meditation.service';
import { NotificationService } from './services/notification.service';
import { StaticFilesService } from './services/static-files.service';
import { UpdateService } from './services/update.service';
import { UsersService } from './services/users.service';
import { UtilsService } from './services/utils.service';
import { PartnersComponent } from './components/partners/partners.component';
import { VoicesComponent } from './components/voices/voices.component';
import { DragDropComponent } from './components/drag-drop/drag-drop.component';

const appRoutes: Routes = [
    {path: '', component: AppComponent},
    {path: 'courses', component: CoursesComponent},
    {path: 'partners', component: PartnersComponent},
    {path: 'login', component: LoginComponent},
    {path: 'users', component: UsersComponent},
    {path: 'statistics', component: StatisticsComponent},
    {path: 'update', component: UpdateComponent},


    {path: 'meditations', component: MeditationsComponent},
    {path: 'new-course', component: NewCourseComponent},
    {path: 'new-meditation', component: NewMeditationComponent},
    {path: 'static-files', component: StaticFilesComponent},
    {path: 'voices', component: VoicesComponent},
    {path: 'notification', component: NotificationComponent},
];

registerLocaleData(localeCs, 'cz-CZ');

@Injectable()
export class DefaultIntl extends OwlDateTimeIntl {
    /** A label for the cancel button */
    public cancelBtnLabel: string = 'Zrušit';

    /** A label for the set button */
    public setBtnLabel: string = 'Potvrdit';
}

export const url = '';
export const apiUrl: string = url + '/api';

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        MeditationsComponent,
        NewMeditationComponent,
        CoursesComponent,
        TruncatePipe,
        CoursesComponent,
        NewCourseComponent,
        LoginComponent,
        UsersComponent,
        StaticFilesComponent,
        StatisticsComponent,
        UsersWithCoursePipe,
        ExperiencePipe,
        NoTestUserPipe,
        UpdateComponent,
        NotificationComponent,
        TableSorterComponent,
        PartnersComponent,
        VoicesComponent,
        DragDropComponent,
    ],
    imports: [
        BrowserModule,
        NgxPaginationModule,
        BrowserAnimationsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        HttpClientModule,
        HttpModule,
        FormsModule,
        NgxDataTableModule,
        ReactiveFormsModule,
        LoadingModule,
        TabsModule.forRoot(),
        IonicStorageModule.forRoot(),
        ModalModule.forRoot(),
        RouterModule.forRoot(appRoutes),
        ProgressHttpModule,
        NgxDaterangepickerMd.forRoot(),
    ],
    providers: [
        {provide: OwlDateTimeIntl, useClass: DefaultIntl},
        {provide: LOCALE_ID, useValue: 'cz-CZ'},
        CoursesService,
        MeditationService,
        UsersService,
        StaticFilesService,
        UpdateService,
        NotificationService,
        UtilsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
