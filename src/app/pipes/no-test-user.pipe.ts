import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'noTestUser'
})
export class NoTestUserPipe implements PipeTransform {

    public transform(items: any, args?: any): any {
        if (!items) {
            return items;
        }
        return items.filter((item: any) => item.roles.indexOf('TEST') === -1);
    }
}
