import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'usersWithCourse'
})
export class UsersWithCoursePipe implements PipeTransform {

    public transform(items: Array<any>, courseId: string): any {
        if (!items) {
            return items;
        }
        const usersWithCourse: any = items.filter((item: any) => item.coursesOwned.length > 0);
        const res: any = usersWithCourse.filter((item: any) => item.coursesOwned.map((e: any) => e.id).indexOf(courseId) >= 0);
        return res;
    }
}
