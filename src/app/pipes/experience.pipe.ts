import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'experience'
})
export class ExperiencePipe implements PipeTransform {

    public transform(value: any, args?: any): any {
        switch (value) {
            case 'LOTS_OF_EXPERIENCE':
                return 'Spoustu';
            case 'MILD_EXPERIENCE':
                return 'Jen trochu';
            case 'NO_EXPERIENCE':
                return 'Nemám';
        }
        return '';
    }
}
