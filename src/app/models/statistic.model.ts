import { CourseStatistics } from './course-statistic.model';

export class UserStatistics {
    public id: number;
    public email: string;
    public coursesSignedIn: number;
    public coursesFinished: number;
    public lessonsSignedIn: number;
    public lessonsFinished: number;
    public lessonsUnfinished: number;
    public totalMeditationTime: number;
    public courses: Array<CourseStatistics>;
}


