import { Lesson } from './lesson.model';

export class Course {
    public type: string;
    public id: string;
    public free: boolean;
    public name: string;
    public description: string = '';
    public subtitle: string;
    public image: string;
    public thumbnail: string = '';
    public intro: Intro;
    public tags: Array<any> = [];
    public lessons: Array<Lesson>;
    public languages: Array<string>;
}

export class Intro {
    public text: string = '';
    public video: string = '';
}

export const dtoToCourse = (dto: any): Course => {
    const result: Course = {
        ...dto,
        lessons: dto.lessons.map(l => ({
            ...l,
            source: l.source.map(s => ({
                ...s,
                lecturer: s.lecturer ? s.lecturer.id : null,
            }))
        }))
    };
    return result;
};
