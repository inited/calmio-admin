import { LessonsFinished } from './lessonsFinished.model';

export interface Courses {
    id: string;
    lessonsFinished: Array<LessonsFinished>;
}
