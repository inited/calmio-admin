export class PartnerToSendModel {
    public id: string;
    public name: string;
    public address: string;

    public constructor(id: string, name: string, address: string) {
        this.id = id;
        this.name = name;
        this.address = address;
    }
}
