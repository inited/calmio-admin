export interface Notification {
    id: number;
    createdAt: Date;
    title: string;
    message: string;
    test: boolean;
}
