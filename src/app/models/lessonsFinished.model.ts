export interface LessonsFinished {
    id: string;
    time: number;
    timestamp: number;
}
