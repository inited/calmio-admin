export interface Voice {
    id: number;
    name: string;
    gender: 'man' | 'woman';
    previewUrl: string;
}
