export class LessonStatisticModel {
    public id: number;
    public completed: boolean;
    public lessonDurationSeconds: number;
    public lessonTime: number;
    public order: number;
}


