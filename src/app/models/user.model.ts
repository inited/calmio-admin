import { Courses } from './courses.model';

export class UserModel {
    public roles: Array<string> = [];
    public name: string;
    public surname: string;
    public email: boolean;
    public installReason: string;
    public experience: string;
    public fullVersionExporation: number;
    public coursesOwned: Array<Courses>;
}


