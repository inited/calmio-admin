import { Source } from './source.model';

export class Lesson {
    public id: string;
    public name: string;
    public order: number;
    public source: Array<Source> = [];
    public description: string;
    public type: string;
    public intro?: {
        text?: any,
        video?: string;
    };

    constructor() {
        this.id = '';
        this.intro = {
            text: '',
            video: '',
        };
        this.type = 'audio';
    }
}
