export interface Option {
    value: boolean;
    text: string;
}
