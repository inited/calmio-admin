export interface Response {
    status: string;
    // any because of multiple types which can be assigned cause errors
    data: any;
}
