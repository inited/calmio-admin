import { Code } from './code.model';
import { CodeUsage } from './codeUsage.model';

export class Partner {
    public id: string;
    public created: number;
    public name: string;
    public address: string;
    public code: Code;
    public usage: CodeUsage;

    public constructor() {
        this.code = new Code();
        this.usage = new CodeUsage();
    }
}
