export class Source {
    public title: string;
    public src: string;
    public voice: string;
    public lecturer: number;

    constructor(title: string, src: string) {
        this.title = title;
        this.src = src;
        this.voice = 'woman';
        this.lecturer = null;
    }
}
