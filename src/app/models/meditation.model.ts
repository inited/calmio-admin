import { Course } from './course.model';
export class Meditation extends Course {
    public isTodayDailyMeditation: boolean;
    public tomorrowPreferredDailyMeditation: boolean;
}

export const dtoToMeditation = (dto: any): Meditation => {
    const result: Meditation = {
        ...dto,
        lessons: dto.lessons.map(l => ({
            ...l,
            source: l.source.map(s => ({
                ...s,
                lecturer: s.lecturer ? s.lecturer.id : null,
            }))
        }))
    };
    return result;
};
