import { LessonStatisticModel } from './lesson-statistic.model';

export class CourseStatistics {
    public id: number;
    public name: string;
    public lessons: Array<LessonStatisticModel>;
}


