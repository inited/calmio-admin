import { CodeUsage } from './codeUsage.model';

export class Code {
    public name: string;
    public created: number;
    public value: number;
    public usage: CodeUsage;

    constructor() {
        this.usage = new CodeUsage();
    }
}
