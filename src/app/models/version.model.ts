export interface Version {
    latestVersion: string;
    forceUpdateVersion: string;
}
