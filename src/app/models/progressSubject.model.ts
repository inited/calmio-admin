import { Subject } from 'rxjs/Subject';

export interface ProgressSubject {
    subject: Subject<number>;
    working: boolean;
}
