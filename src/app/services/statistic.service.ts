import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { UtilsService } from './utils.service';


@Injectable({
    providedIn: 'root'
})
export class StatisticService {

    constructor(private http: HttpClient, private utilsService: UtilsService) {
    }

    public async getUserStatistics(params: any = null): Promise<any> {
        console.log(params);
        try {
            const options = this.utilsService.getAuthHeader();
            options.params = params;
            const response: any = await this.http.get(
                `${environment.url}/api/admin/statistics/users`,
                options
            ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async getUserStatisticsExport(): Promise<any> {
        try {
            const options = this.utilsService.getAuthHeader('', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            options[`responseType`] = 'blob';
            const response: any = await this.http.get(
                `${environment.url}/api/admin/statistics/export`,
                options
            ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }
}
