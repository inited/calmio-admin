import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { dtoToMeditation, Meditation } from '../models/meditation.model';
import { Response } from '../models/response.model';
import { UtilsService } from './utils.service';

@Injectable()
export class MeditationService {
    constructor(private http: HttpClient,
                private utilsService: UtilsService) {
    }

    public async getCourses(): Promise<Response> {
        try {
            const response: any = await this.http.get<any>(
                `${environment.url}/api/admin/daily-meditation`,
                this.utilsService.getAuthHeader()
            )
                .pipe(map((resp: any) => resp.map(c => dtoToMeditation(c))))
                .toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async getCourseById(id: string): Promise<Response> {
        try {
            const response: any = await this.http.get<any>(
                `${environment.url}/api/admin/daily-meditation/${id}`,
                this.utilsService.getAuthHeader()
            ).pipe(map((resp: any) => dtoToMeditation(resp)))
                .toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async newCourse(course: any): Promise<Response> {
        try {
            const body = {
                name: course.name,
                subtitle: course.subtitle,
                free: course.free,
                description: course.description,
                lessons: course.lessons,
                tags: course.tags,
                titlePhoto: course.image,
                languages: course.languages,
                thumbnail: course.thumbnail
            };
            const response: any = await this.http
                .post(
                    `${environment.url}/api/admin/daily-meditation`, JSON.stringify(body),
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async deleteCourse(id: string): Promise<Response> {
        try {
            const response: any = await this.http
                .delete(
                    `${environment.url}/api/admin/daily-meditation/${id}`,
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async updateCourse(course: Meditation): Promise<Response> {
        try {
            const body = {
                id: course.id,
                name: course.name,
                subtitle: course.subtitle,
                free: course.free,
                description: course.description,
                lessons: course.lessons,
                tags: course.tags,
                titlePhoto: course.image,
                languages: course.languages,
                thumbnail: course.thumbnail
            };
            const response: any = await this.http
                .put(
                    `${environment.url}/api/admin/daily-meditation/${course.id}`,
                    JSON.stringify(body),
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }

    }

    public async scheduleTomorrow(id: string): Promise<Response> {
        try {
            const response: any = await this.http
                .put(
                    `${environment.url}/api/admin/daily-meditation/${id}/tomorrow`,
                    {},
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }
}
