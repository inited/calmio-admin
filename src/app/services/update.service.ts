import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { Response } from '../models/response.model';
import { Version } from '../models/version.model';
import { UtilsService } from './utils.service';

@Injectable()
export class UpdateService {

    constructor(private http: HttpClient,
                private router: Router,
                private utilsService: UtilsService) {
    }

    public async getCurrentVersion(): Promise<Response> {
        try {
            const response: any = await this.http.get<Version>(
                `${environment.url}/api/app-version`,
                this.utilsService.getAuthHeader()
            ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async sendUpdate(currentVersions: Version, updatedVersions: Version): Promise<Response> {
        const body = this.getFillEmptyVersionValues(currentVersions, updatedVersions);
        try {
            const response: any = await this.http
                .put(
                    `${environment.url}/api/admin/app-version`,
                    JSON.stringify(body),
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    private getFillEmptyVersionValues(currentVersions: any, updatedVersions: any): Version {
        if (!updatedVersions.latestVersion) {
            updatedVersions.latestVersion = currentVersions.latestVersion;
        }
        if (!updatedVersions.forceUpdateVersion) {
            updatedVersions.forceUpdateVersion = currentVersions.forceUpdateVersion;
        }
        return updatedVersions;
    }
}
