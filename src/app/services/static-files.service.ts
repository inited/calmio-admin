import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Response } from '../models/response.model';
import { Static } from '../models/static.model.model';
import { UsersService } from './users.service';
import { UtilsService } from './utils.service';

const ABOUT = 'ABOUT';
const TERMS = 'TERMS_AND_CONDITIONS';

@Injectable()
export class StaticFilesService {

    static decodeEntities(encodedString: string): string {
        const textArea = document.createElement('textarea');
        textArea.innerHTML = encodedString;
        return textArea.value;
    }

    constructor(private http: HttpClient, private usersService: UsersService, private utilsService: UtilsService) {
    }

    public async getAboutStaticHtml(language: string): Promise<Response> {
        try {
            const httpOptions = this.utilsService.getAuthHeader('');
            httpOptions.params = new HttpParams().set('id', ABOUT).set('language', language);
            const response: any = await this.http.get<Static>(
                `${environment.url}/api/admin/static`,
                httpOptions).pipe(
                    map((res: any) => StaticFilesService.decodeEntities(res.html))
            ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async getTermsStaticHtml(language: string): Promise<Response> {
        try {
            const httpOptions = this.utilsService.getAuthHeader('');
            httpOptions.params = new HttpParams().set('id', TERMS).set('language', language);
            const response: any = await this.http.get<Static>(
                `${environment.url}/api/admin/static`,
                httpOptions)
                .pipe(
                    map((res: any) => StaticFilesService.decodeEntities(res.html))
                ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async saveAboutStaticHtml(data: string, language: string): Promise<Response> {
        try {
            const body = {
                id: ABOUT,
                html: data,
                language
            };
            const response: any = await this.http
                .post(
                    `${environment.url}/api/admin/static`,
                    JSON.stringify(body),
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async saveTermsStaticHtml(data: string, language: string): Promise<Response> {
        try {
            const body = {
                id: TERMS,
                html: data,
                language
            };
            const response: any = this.http
                .post(
                    `${environment.url}/api/admin/static`,
                    JSON.stringify(body),
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }
}
