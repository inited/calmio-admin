import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Code } from '../models/code.model';
import { Partner } from '../models/partner.model';
import { PartnerToSendModel } from '../models/partnerToSend.model';
import { Response } from '../models/response.model';
import { UsersService } from './users.service';
import { UtilsService } from './utils.service';


@Injectable({
    providedIn: 'root'
})
export class PartnerService {

    constructor(private http: HttpClient,
                private usersService: UsersService,
                private utilsService: UtilsService) {
    }

    public async getPartners(): Promise<Response> {
        try {
            const response: any = await this.http.get<Array<Partner>>(
                `${environment.url}/api/admin/partners`,
                this.utilsService.getAuthHeader()
            ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async deletePartner(id: string): Promise<Response> {
        try {
            const response: any = await this.http
                .delete(
                    `${environment.url}/api/admin/partner/${id}`,
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async savePartner(partner: Partner, modify: boolean = false): Promise<Response> {
        try {
            let response: any;
            const partnerToSend: PartnerToSendModel = new PartnerToSendModel(partner.id, partner.name, partner.address);
            if (!modify) {
                response = await this.http.post(`${environment.url}/api/admin/partner`,
                    partnerToSend,
                    this.utilsService.getAuthHeader()
                ).toPromise();
            } else {
                response = await this.http.put(`${environment.url}/api/admin/partner/${partner.id}`,
                    partnerToSend,
                    this.utilsService.getAuthHeader()
                ).toPromise();
            }
            console.log('odpověď:', response);
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async saveNewCode(code: Code, id: string): Promise<Response> {
        try {
            const response: any = await this.http
                .post(
                    `${environment.url}/api/admin/partner/${id}/code-create`,
                    code,
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response.data
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async saveUpdatedCode(code: Code, id: string): Promise<Response> {
        try {
            const response: any = await this.http
                .put(
                    `${environment.url}/api/admin/partner/${id}/code/${code.name}`,
                    code,
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response.data
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async deleteCode(code: Code, id: string): Promise<Response> {
        try {
            await this.http
                .delete(
                    `${environment.url}/api/admin/partner/${id}/code/${code.name}`,
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: null
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }
}
