import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import { UserModel } from '../models/user.model';

@Injectable()
export class UsersService {

    private headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    private options: { headers: HttpHeaders } = {
        headers: this.headers
    };
    private token: string;


    constructor(private http: HttpClient, private storage: Storage) {
    }

    private static handleErrors(err: any): { status: string } {
        console.log(err);
        if (err.status === 400) {
            return {
                status: 'notActivated'
            };
        } else if (err.status === 403) {
            return {
                status: 'wrongCredentials'
            };
        } else if (err.status === 404) {
            return {
                status: 'wrongCredentials'
            };
        } else {
            return {
                status: 'err'
            };
        }
    }

    private static getHttpHeaders(token: string): HttpHeaders {
        return new HttpHeaders({
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json'
        });
    }

    public async login(email: string, password: string): Promise<any> {
        try {
            const response: any = await this.http.post(
                `${environment.url}/api/user/login-password`,
                'email=' + email + '&password=' + password
                , this.options
            ).toPromise();
            console.log('odpověď:', response);
            this.setToken(response.token);
            return {
                status: 'ok',
                user: response
            };
        } catch (err) {
            return UsersService.handleErrors(err);
        }
    }

    public async updateUser(user: any): Promise<any> {
        const userAdmin: any = await this.storage.get('user');
        try {
            const data: string = JSON.stringify(user);
            const options = {
                headers: UsersService.getHttpHeaders(userAdmin.token)
            };
            const response: any = await this.http.put(
                `${environment.url}/api/admin/users`, data, options
            ).toPromise();
            console.log('odpověď:', response);
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return UsersService.handleErrors(err);
        }
    }

    public async getUsers(params: any = null): Promise<any> {
        const user: any = await this.storage.get('user');
        try {
            const options = {
                headers: UsersService.getHttpHeaders(user.token),
                params
            };
            const response: any = await this.http.get(
                `${environment.url}/api/admin/users`,
                options
            ).toPromise();
            console.log('odpověď:', response);
            return {
                status: 'ok',
                users: response
            };
        } catch (err) {
            return UsersService.handleErrors(err);
        }
    }

    public async deleteUser(user: UserModel): Promise<any> {
        const userAdmin: any = await this.storage.get('user');
        try {
            const data: any = {
                email: user.email
            };
            const options = {
                headers: UsersService.getHttpHeaders(userAdmin.token),
                body: data
            };
            const response: any = await this.http.delete(
                `${environment.url}/api/admin/users`,
                options
            ).toPromise();
            console.log('odpověď:', response);
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            UsersService.handleErrors(err);
        }
    }

    public setToken(token: string): void {
        this.token = token;
        localStorage.setItem('token', token);
    }

    public getToken(): string {
        return this.token;
    }

    public logout(): void {
        this.storage.set('user', undefined);
        localStorage.setItem('token', '');
        this.token = undefined;
    }
}
