import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Course, dtoToCourse } from '../models/course.model';
import { Response } from '../models/response.model';
import { UtilsService } from './utils.service';

@Injectable()
export class CoursesService {
    constructor(private http: HttpClient,
                private utilsService: UtilsService) {
    }

    public async getCourses(): Promise<Response> {
        try {
            const response: any = await this.http.get<any>(
                `${environment.url}/api/admin/courses`,
                this.utilsService.getAuthHeader()
            )
                .pipe(map((resp: any) => {
                    console.log(resp);
                    return resp.map(c => dtoToCourse(c));
                }))
                .toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async getCourseById(id: string): Promise<Response> {
        try {
            const response: any = await this.http.get<Course>(
                `${environment.url}/api/admin/courses/${id}`,
                this.utilsService.getAuthHeader()
            )
                .pipe(map((resp: any) => dtoToCourse(resp)))
                .toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async newCourse(course: any): Promise<Response> {
        try {
            const body = {
                name: course.name,
                subtitle: course.subtitle,
                free: course.free,
                description: course.description,
                lessons: course.lessons,
                tags: course.tags,
                titlePhoto: course.image,
                languages: course.languages,
                thumbnail: course.thumbnail
            };
            const response: any = await this.http
                .post(
                    `${environment.url}/api/admin/courses`, JSON.stringify(body),
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async deleteCourse(id: string): Promise<Response> {
        try {
            const response: any = await this.http
                .delete(
                    `${environment.url}/api/admin/courses/${id}`,
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async updateCourse(course: Course): Promise<Response> {
        try {
            const body = {
                id: course.id,
                name: course.name,
                subtitle: course.subtitle,
                free: course.free,
                description: course.description,
                lessons: course.lessons,
                tags: course.tags,
                titlePhoto: course.image,
                languages: course.languages,
                thumbnail: course.thumbnail
            };
            const response: any = await this.http
                .put(
                    `${environment.url}/api/admin/courses/${course.id}`,
                    JSON.stringify(body),
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }

    }
}
