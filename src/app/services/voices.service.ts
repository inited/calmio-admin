import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Response } from '../models/response.model';
import { Voice } from '../models/voice.model';
import { UtilsService } from './utils.service';


@Injectable({
    providedIn: 'root'
})
export class VoicesService {

    constructor(
        private http: HttpClient,
        private utilsService: UtilsService
    ) {
    }

    public async getAll(): Promise<Response> {
        try {
            const response: any = await this.http
                .get<Voice[]>(
                    `${environment.url}/api/admin/lecturers`,
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async delete(id: number): Promise<Response> {
        try {
            const response: any = await this.http
                .delete(
                    `${environment.url}/api/admin/lecturer/${id}`,
                    this.utilsService.getAuthHeader()
                ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async create(voice: Voice): Promise<Response> {
        try {
            const response = await this.http.post(
                `${environment.url}/api/admin/lecturer`,
                voice,
                this.utilsService.getAuthHeader()
            ).toPromise();
            console.log('odpověď:', response);
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async save(voice: Voice): Promise<Response> {
        try {
            const response = await this.http.put(
                `${environment.url}/api/admin/lecturer/${voice.id}`,
                voice,
                this.utilsService.getAuthHeader()
            ).toPromise();
            console.log('odpověď:', response);
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }
}
