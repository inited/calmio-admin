import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Notification } from '../models/notification.model';
import { UtilsService } from './utils.service';

@Injectable()
export class NotificationService {

    constructor(private http: HttpClient, private utilsService: UtilsService) {
    }

    public async getNotifications(): Promise<any> {
        try {
            const response: any = await this.http.get<Array<Notification>>(
                `${environment.url}/api/admin/push-notifications`,
                this.utilsService.getAuthHeader('')
            ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async sendNotification(notification: Notification): Promise<any> {
        try {
            const response: any = await this.http.post(
                `${environment.url}/api/admin/push-notification`,
                notification,
                this.utilsService.getAuthHeader('')
            ).toPromise();
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }

    public async deleteNotification(id: number): Promise<any> {
        try {
            const response: any = await this.http.delete(
                `${environment.url}/api/admin/push-notification/${id}`,
                this.utilsService.getAuthHeader('')
            ).toPromise();
            console.log('odpověď:', response);
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            return this.utilsService.handleError(err);
        }
    }
}
