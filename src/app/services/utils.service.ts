import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ProgressHttp } from 'angular-progress-http';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../environments/environment';
import { Language } from '../models/language.model';
import { Response } from '../models/response.model';
import { UsersService } from './users.service';

@Injectable()
export class UtilsService {

    constructor(private usersService: UsersService, private router: Router, private progressHttp: ProgressHttp) {
    }

    public getLanguages(): Array<Language> {
        return [
            {label: 'Čeština', value: 'cs'},
            {label: 'Slovenština', value: 'sk'},
            {label: 'Polština', value: 'pl'},
            {label: 'Maďarština', value: 'hu'},
            {label: 'Angličtina', value: 'en'},
        ];
    }

    public handleError(err: any): Response {
        console.log('error', err);
        if (err.status === 401) {
            this.router.navigate(['/login']);
        } else {
            let error: any;
            if (err && err.hasOwnProperty('error') && err.error.hasOwnProperty('error')) {
                error = err.error.error;
            } else if (err && err.hasOwnProperty('error')) {
                error = err.error;
            } else {
                error = {
                    message: 'Při zpracování requestu došlo k chybě.'
                };
                if (err.hasOwnProperty('statusText')) {
                    error.message += ` Chybová hláška: ${err.statusText}`;
                }
            }
            return {
                status: 'err',
                data: error
            };
        }
    }

    public getAuthHeader(contentType: string = 'application/json', accept: string = 'application/json'): any {
        const options: any = {
            headers: {
                Authorization: `Bearer ${this.usersService.getToken()}`,
            }
        };
        if (contentType) {
            options.headers['Content-Type'] = contentType;
        }
        if (accept) {
            options.headers[`Accept`] = accept;
        }
        return options;
    }

    public async uploadFile(file: File, subject: Subject<number>): Promise<any> {
        try {
            const formData: FormData = new FormData();
            formData.append('file', file, file.name);
            const response: any = await this.progressHttp
                .withUploadProgressListener((progress: any) => {
                    // this.sendSubject(progress.percentage);
                    subject.next(progress.percentage);
                })
                .post(
                    `${environment.url}/api/admin/file-upload`,
                    formData,
                    this.getAuthHeader(''))
                .map(res => res.json()
                ).toPromise();

            console.log('odpověď:', response);
            return {
                status: 'ok',
                data: response
            };
        } catch (err) {
            this.handleError(err);
        }
    }
}
